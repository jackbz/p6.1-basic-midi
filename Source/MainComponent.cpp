/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (600, 100);
    
    audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    
    addAndMakeVisible(messageTypeLabel);
    messageTypeLabel.getTextValue() = "Message Type";
    
    addAndMakeVisible(channelLabel);
    channelLabel.getTextValue() = "Channel";
    
    addAndMakeVisible(numberLabel);
    numberLabel.getTextValue() = "Number";
    
    addAndMakeVisible(velocityLabel);
    velocityLabel.getTextValue() = "Velocity";
    
    messageType.addItem ("Note", 1);
    messageType.addItem ("Control", 2);
    messageType.addItem ("Program", 3);
    messageType.addItem ("Pitch Bend", 4);
    addAndMakeVisible(messageType);
    
    addAndMakeVisible(channel);
    channel.setRange(1, 16, 1);
    channel.setSliderStyle(Slider::IncDecButtons);
    channel.addListener(this);
    
    addAndMakeVisible(number);
    number.setSliderStyle(Slider::IncDecButtons);
    number.setRange(0, 127, 1);
    number.addListener(this);
    
    addAndMakeVisible(velocity);
    velocity.setSliderStyle(Slider::IncDecButtons);
    velocity.setRange(0, 127, 1);
    velocity.addListener(this);
    
    addAndMakeVisible(send);
    send.setButtonText("Send");
    send.addListener(this);
    
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    
    messageTypeLabel.setBounds(0, 0, getWidth()/5, 20);
    channelLabel.setBounds(getWidth()/5, 0, getWidth()/5, 20);
    numberLabel.setBounds((getWidth()/5)*2, 0, getWidth()/5, 20);
    velocityLabel.setBounds((getWidth()/5)*3, 0, getWidth()/5, 20);
    
    
    messageType.setBounds(0, 50, getWidth()/5, 20);
    channel.setBounds(getWidth()/5, 50, getWidth()/5, 20);
    number.setBounds((getWidth()/5)*2, 50, getWidth()/5, 20);
    velocity.setBounds((getWidth()/5)*3, 50, getWidth()/5, 20);
    send.setBounds((getWidth()/5)*4, 50, getWidth()/5, 20);
    
}

void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage& message)
{
    DBG ("Message Received!");
    
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &send)
        outputMessage.noteOn(channel.getValue(), number.getValue(), 100.f);
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (outputMessage);
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &channel)
    {
        outputMessage.setChannel(channel.getValue());
        DBG (channel.getValue());
        DBG (outputMessage.getChannel());
    }
    else if (slider == &number)
    {
        outputMessage.setNoteNumber(number.getValue());
        DBG (number.getValue());
        DBG (outputMessage.getNoteNumber());
    }
    else if (slider == &velocity)
    {
        outputMessage.setVelocity(velocity.getValue());
        DBG (velocity.getValue());
        DBG (outputMessage.getVelocity());
    }
}

void MainComponent::comboBoxChanged (ComboBox* comboBox)
{
   
}
